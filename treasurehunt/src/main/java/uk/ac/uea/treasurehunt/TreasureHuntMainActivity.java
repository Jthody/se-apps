package uk.ac.uea.treasurehunt;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.uea.Framework.AndroidLocation;
import uk.ac.uea.Framework.Compass;
import uk.ac.uea.Framework.EndActivity;

/**
 * This is the main class for the Treasure Hunt app. It uses the users current location to determine
 * how far away from the target location they are and changes the screen colour accordingly.
 */
public class TreasureHuntMainActivity extends uk.ac.uea.Framework.MainActivity {

    ArrayList<AndroidLocation> treasureHuntLocations;
    int targetLocationIndex;
    AndroidLocation endLocation;
    int hintTimes;
    boolean setUp;
    TextView txtHint;
    TextView txtDistance;
    TextView txtColour;
    Button btnNext;
    boolean startedHunt;
    private ImageView image;
    FrameLayout frameLayoutBalance;

    /**
     * Android Activity onCreate method that is called when the Activity is create.
     * This method sets up the app ready for use.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_treasure_hunt_main);

        //build the app toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        //Add the map fragment to the app
        addMapFragment();
        //Set the flag initially to false to allow the map to set up
        startedHunt = false;
        //Set the flag of the hunt being set up ti false to allow the map to be set up
        setUp = false;

        //Create the list of locations
        treasureHuntLocations = new ArrayList();

        //Set the index to 0 for the first location in the list
        targetLocationIndex = 0;

        //populate the list of locations
        populateList();

        txtHint = (TextView) findViewById(R.id.txtHint);

        txtDistance = new TextView(this);

        txtDistance = (TextView) findViewById(R.id.txtDistance);

        txtColour = new TextView(this);

        txtColour = (TextView) findViewById(R.id.txtColour);

        btnNext = (Button) findViewById(R.id.btnNext);

        image = (ImageView) findViewById(R.id.imageViewCompass);

        //Get the compass object from the framework
        compass = Compass.getInstance();

        //get the view
        View view = findViewById(R.id.layout);
        //get the framelayout
        frameLayoutBalance = (FrameLayout) view.findViewById(R.id.rec);

        //Create the end location for the treasure hunt
        endLocation = new TreasureHuntLocation(52.621597, 1.242026, "LCR", "The Lower Common Room. Home of Student Nightlife.", "The venue where student club nights are held.");


        //Button that shows compass
        findViewById(R.id.btnShow).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                //show compass for 3 seconds
                new CountDownTimer(3000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        //hide text
                        txtColour.setVisibility(View.INVISIBLE);
                        //show compass
                        compass.setVisible();
                        findViewById(R.id.btnShow).setEnabled(false);
                    }

                    public void onFinish() {
                        //show text
                        txtColour.setVisibility(View.VISIBLE);
                        //hide compass
                        compass.setInvisible();
                        findViewById(R.id.btnShow).setEnabled(true);
                        //increment the number of times the user has used a hint
                        hintTimes++;
                    }
                }.start();
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);

        //Check if the map is set but the treasure hunt is not yet set up
        if (map != null && !setUp) {
            setUpTreasureHunt();
            setUp = true;
        }

        //Check if the screen colour needs changing
        updateColour();

        //Check if the user at the target location
        if (map.isUserAtLocation(treasureHuntLocations.get(targetLocationIndex)) && startedHunt) {
            //temporarily stop the location updates
            stopLocationUpdates();

            //Check if there is still locations left in the hunt
            if (targetLocationIndex < treasureHuntLocations.size()) {
                //Set the description of the target once the user has arrived
                txtHint.setText(((TreasureHuntLocation) treasureHuntLocations.get(targetLocationIndex)).getDescription());
                //Button to start route to next location
                btnNext.setText("Next Location");
                btnNext.setVisibility(View.VISIBLE);

                //Handle next location button
                findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        startLocationUpdates();
                        nextTarget();
                    }
                });
            }
            //Finished tour as all locations have been visted
            else {
                txtHint.setText("Congratulations. You have finished the treasure hunt and used " + hintTimes + " hints!");
                btnNext.setText("End App");
                btnNext.setVisibility(View.VISIBLE);

                //Handle end app button
                findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent = new Intent(TreasureHuntMainActivity.this, EndActivity.class);
                        finish();
                        startActivity(intent);
                    }
                });
            }
        }
    }

    /**
     * Method to set the users target location to the next location in the list
     */
    private void nextTarget() {

        //increment the index for the target location list
        targetLocationIndex++;
        txtHint.setText(((TreasureHuntLocation) treasureHuntLocations.get(targetLocationIndex)).getQuestion());
        //Change the compass to point to the new location
        compass.changeDestination(treasureHuntLocations.get(targetLocationIndex));
    }

    /**
     * Method that initially sets up the treasure hunt
     */
    private void setUpTreasureHunt() {

        txtHint.setText(((TreasureHuntLocation) treasureHuntLocations.get(targetLocationIndex)).getQuestion());
        //Flag to tell location updates that the hunt has been started
        startedHunt = true;
        //Set up the compass to point to the first location
        compass.setupCompass(currentAndroidLocation, treasureHuntLocations.get(targetLocationIndex), image);
        compassEnabled = true;

    }

    /**
     * Methods that updates the colour of the screen depending how far away the user is
     * from the target location
     */
    private void updateColour() {

        //Get the distance value
        double distance = map.distanceToLocation(treasureHuntLocations.get(targetLocationIndex));

        txtDistance.setText(Double.toString(distance));

        //choose colour based on distance
        if (distance < 5) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.red));
            txtColour.setText("Congratulations! You've found it.");
        } else if (distance < 10) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.lightRed));
            txtColour.setText("You're so close you can almost smell it...");
        } else if (distance < 25) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.mangoOrange));
            txtColour.setText("You're not far away now...");
        } else if (distance < 50) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.almond));
            txtColour.setText("You're getting very warm...");
        } else if (distance < 75) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.lightBlue));
            txtColour.setText("You're getting warmer...");
        } else if (distance < 100) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.blue));
            txtColour.setText("You're cold, but closer...");
        } else if (distance < 125) {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.darkBlue));
            txtColour.setText("Brr it's pretty cold where you are...");
        } else {
            frameLayoutBalance.setBackgroundColor(ContextCompat.getColor(this.getApplicationContext(), R.color.colorPrimaryDark));
            txtColour.setText("You're a long way off...");
        }

    }

    /**
     * Method to initially populate the list of hunt locations
     */
    private void populateList() {

        treasureHuntLocations.add(new TreasureHuntLocation(52.621106, 1.240650, "UEA Library", "The Library contains over 800,000 books and journals, 1,200 study spaces, over 260 computers for student use and 24 hour open access to the IT facility.", "This building is where you can borrow books."));

        treasureHuntLocations.add(new TreasureHuntLocation(52.622810, 1.242182, "University Drive Bus Stop A", "For buses: 25, 25A and X25 to Norwich city centre and Rail Station", "Main bus stop to the city and train station."));

        treasureHuntLocations.add(new TreasureHuntLocation(52.621716, 1.235509, "Elizabeth Fry Hub", "This Hub manages student administration for undergraduate (UG) and postgraduate taught (PGT) students.", "The student administration hub named after woman who dedicated much of her adult life to helping those in need."));

        treasureHuntLocations.add(new TreasureHuntLocation(52.623356, 1.246764, "University Medical Centre", "UEA has a Medical Centre to facilitate the students on its campus.", "You would go here for a health checkup."));

        treasureHuntLocations.add(endLocation);

    }
}
