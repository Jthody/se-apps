package uk.ac.uea.treasurehunt;

import uk.ac.uea.Framework.POI;

/**
 * Created by joshthody on 22/12/2015.
 */
public class TreasureHuntLocation extends POI {

    String question;

    public TreasureHuntLocation(double latitude, double longitude, String name, String description, String question)
    {
        super(latitude, longitude, name, description);

        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return name;
    }
}
