package uk.ac.uea.treasurehunt;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import uk.ac.uea.Framework.Audio;
import uk.ac.uea.Framework.SoundResource;


public class TreasureHuntStartActivity extends AppCompatActivity {

    TextView splashHeading;
    Audio MyAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SoundResource sound = new SoundResource(this);
        sound.load("beep.mp3");
        sound.play();


        setContentView(uk.ac.uea.Framework.R.layout.splashloading);

        // TextView that will show the application name
        splashHeading = (TextView) findViewById(R.id.textView);

        splashHeading.setText("Treasure Hunt");



        final ImageView imageview = (ImageView) findViewById(uk.ac.uea.Framework.R.id.imageView);
        final ImageView loading = (ImageView) findViewById(uk.ac.uea.Framework.R.id.load);

        final Animation rotationanimation = AnimationUtils.loadAnimation(getBaseContext(), uk.ac.uea.Framework.R.anim.rotation);



        loading.startAnimation(rotationanimation);
        rotationanimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
            // Plays sound on animation start
            @Override
            public void onAnimationStart(Animation animation) {
                sound.play();
            }
            // At animation end the sound is stopped
            @Override
            public void onAnimationEnd(Animation animation) {

                finish();
                Intent intent = new Intent(TreasureHuntStartActivity.this, TreasureHuntMainActivity.class);
                sound.stop();
                startActivity(intent);

            }


        });





    }
}
