package uk.ac.uea.parkingaid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import uk.ac.uea.Framework.Audio;
import uk.ac.uea.Framework.Music;
import uk.ac.uea.Framework.SimpleText;
import uk.ac.uea.Framework.Sound;
import uk.ac.uea.Framework.SoundResource;

import uk.ac.uea.Framework.StartActivity;
import uk.ac.uea.Framework.AndroidAudio;


public class ParkingAidStartActivity extends Activity {

    TextView splashHeading;
    Audio MyAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SoundResource sound = new SoundResource(this);
        sound.load("beep.mp3");
        sound.play();


        setContentView(uk.ac.uea.Framework.R.layout.splashloading);

        // TextView that will show the application name
        splashHeading = (TextView) findViewById(R.id.textView);
        splashHeading.setText("Parking Aid");



        final ImageView imageview = (ImageView) findViewById(uk.ac.uea.Framework.R.id.imageView);
        final ImageView loading = (ImageView) findViewById(uk.ac.uea.Framework.R.id.load);

        final Animation rotationanimation = AnimationUtils.loadAnimation(getBaseContext(), uk.ac.uea.Framework.R.anim.rotation);



        loading.startAnimation(rotationanimation);
        rotationanimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
            // Plays sound on animation start
            @Override
            public void onAnimationStart(Animation animation) {
                sound.play();
            }
            // At animation end the sound is stopped
            @Override
            public void onAnimationEnd(Animation animation) {
                
                finish();
                Intent intent = new Intent(ParkingAidStartActivity.this, ParkingAidInfoActivity.class);
                startActivity(intent);
                sound.stop();
            }


        });



    }
}
