package uk.ac.uea.parkingaid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import uk.ac.uea.Framework.AndroidLocation;
import uk.ac.uea.Framework.AndroidLocationOriginator;
import uk.ac.uea.Framework.AndroidMap;
import uk.ac.uea.Framework.CareTaker;
import uk.ac.uea.Framework.EndActivity;


/**
 * Main class for the Parking Aid app. It handles the parking and returning to the users car.
 */
public class ParkingAidMainActivity extends uk.ac.uea.Framework.MainActivity {

    AndroidLocation carLocation;
    boolean parkedCar = false;
    boolean walkingBack = false;
    TextView carLocText;
    private ImageView image;
    long timeRemaining;

    SharedPreferences parkingInfo;
    long duration;

    //Objects for the memento pattern to use.
    CareTaker caretaker = new CareTaker();
    AndroidLocationOriginator originator = new AndroidLocationOriginator();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_aid_main);

        //Get the time the user entered they are parking for
        parkingInfo = PreferenceManager.getDefaultSharedPreferences(this);
        duration = parkingInfo.getLong("parkingTime",0);

        timeRemaining = TimeUnit.MILLISECONDS.convert(duration, TimeUnit.HOURS);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        carLocText = (TextView) findViewById(R.id.currentLoc);

        // our compass image
        image = (ImageView) findViewById(R.id.imageViewCompass);

        //Add the map fragment
        addMapFragment();

        //Get the compass
        compass = compass.getInstance();

        //Memento pattern.
        originator.setMyValue(carLocation);
        caretaker.addMemento(originator.saveToMemento());

        //Timer for the park car button, so it doesn't appear until the map has loaded.
        findViewById(R.id.btnPark).postDelayed(new Runnable() {
            public void run() {
                findViewById(R.id.btnPark).setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Determining your location!",
                        Toast.LENGTH_SHORT).show();
            }
        }, 5000);

        //Handle show map button press
        findViewById(R.id.btnShow).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showHideFragment(fragment);
            }
        });

        //Handle show map button press
        findViewById(R.id.btnPark).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                //If car is not parked
                if (originator.getMyValue() == null) {
                    parkCar();

                }
                //If car is parked
                else if (parkedCar && !walkingBack) {

                    returnToCar();
                }


            }
        });

        //Handle the undo button press
        findViewById(R.id.btnUndo).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //If car is parked and location is not null.
                if(parkedCar && carLocation != null){
                    //Restore the originator to a null location.
                    originator.restoreFromMemento( caretaker.getMemento(0) );
                    //Change the button back to park car.
                    ((android.widget.Button) findViewById(R.id.btnPark)).setText("Park Car");
                    //Remove the marker on the map.
                    map.removeMarker();
                    //Change variables back to false and remove text.
                    walkingBack = false;
                    parkedCar = false;
                    carLocText.setText(null);
                }

            }
        });

    }

    /**
     * control what each of the menu buttons do within the parking activity.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            //case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
             //   return true;

            case R.id.timeRemaining:

                //calculating time remaining.
                int seconds = (int) (timeRemaining / 1000) % 60 ;
                int minutes = (int) ((timeRemaining / (1000*60)) % 60);
                int hours   = (int) ((timeRemaining / (1000*60*60)) % 24);

                //Display how much time the user has left
                Toast.makeText(getApplicationContext(), "You have " + Integer.toString(hours) +" Hours, " + Integer.toString(minutes)
                                +" minutes and " + Integer.toString(seconds) + " seconds left in the car park!",
                        Toast.LENGTH_LONG).show();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Method that is called when the phone detects that the user has changed location
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {

        super.onLocationChanged(location);

        //Check if the user is walking back to their car
        //if so then update the route
        if(walkingBack)
            updateRoute();
    }

    /**
     * This method updates the route if the user is walking back to their car
     */
    private void updateRoute()
    {
        if (walkingBack) {
            //update distance from car
            double distance = AndroidMap.distanceBetweenLocations(carLocation, currentAndroidLocation);
            int d = (int) distance;
            ((android.widget.TextView) findViewById(R.id.distance)).setText(String.valueOf(d + "m from car."));


            //Re-draw the route on the map
            map.removePolylines();
            map.routeTo(carLocation, "walking");

            //compass.updateUserLoc(currentAndroidLocation);

            //Check if the user has returned to their car
            if (map.isUserAtLocation(carLocation)) {
                //Stop location updates as user has returned to their car
                stopLocationUpdates();

                //Create alert box to tell user that they have retuned
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                final TextView input = new TextView(this);
                //Remove the marker on the map.
                map.removeMarker();
                Toast.makeText(getApplicationContext(), "We have detected that you have returned to your car. Thank for you using our app!",
                        Toast.LENGTH_LONG).show();
                compass.setInvisible();
                findViewById(R.id.btnFinish).setVisibility(View.VISIBLE);
                //Handle show map button press
                findViewById(R.id.btnFinish).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        showHideFragment(fragment);
                        Intent intent = new Intent(ParkingAidMainActivity.this, EndActivity.class);
                        startActivity(intent);
                    }
                });
                alert.show();
            }
        }
    }

    /**
     * This method parks the users car at their current location
     */
    private void parkCar()
    {
        carLocation = new AndroidLocation(map.getCurrentAndroidLocation());
        originator.setMyValue(carLocation);
        map.addMarker(carLocation, "Your Car is Here!");
        ((android.widget.Button) findViewById(R.id.btnPark)).setText("Return to Car");
        //Make the undo button appear on screen.
        findViewById(R.id.btnUndo).setVisibility(View.VISIBLE);
        findViewById(R.id.btnShow).setVisibility(View.VISIBLE);
        parkedCar = true;

        //Counts down the time they have remaining in the car park
        new CountDownTimer(TimeUnit.MILLISECONDS.convert(duration, TimeUnit.HOURS), 1000) {

            public void onTick(long millisUntilFinished) {
                timeRemaining-=1000;
            }

            public void onFinish() {

            }
        }.start();

        carLocText.setText("Car Location: " + map.getCompleteAddressString(carLocation.getLatitude(), carLocation.getLongitude(), getApplicationContext()));
    }


    /**
     * This method tells the app that the user wants to return to their car
     * and draws the route to the users car on the map
     */
    private void returnToCar()
    {
        compass.setupCompass(currentAndroidLocation, carLocation, image);
        compassEnabled = true;
        compass.setVisible();
        findViewById(R.id.currentLoc).setVisibility(View.INVISIBLE);
        findViewById(R.id.btnShow).setVisibility(View.INVISIBLE);
        //make the undo button go.
        findViewById(R.id.btnUndo).setVisibility(View.GONE);

        ((android.widget.Button) findViewById(R.id.btnPark)).setVisibility(View.GONE);
        map.routeTo(carLocation, "WALKING");

        double distance = AndroidMap.distanceBetweenLocations(carLocation, currentAndroidLocation);
        int d = (int) distance;

        ((android.widget.TextView) findViewById(R.id.distance)).setText(String.valueOf(d + "m from car."));
        walkingBack = true;
    }
}
