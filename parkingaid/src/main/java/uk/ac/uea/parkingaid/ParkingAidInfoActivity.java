package uk.ac.uea.parkingaid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ParkingAidInfoActivity extends AppCompatActivity {

    // Create object of SharedPreferences to use for saving variables.
    SharedPreferences parkingInfo;
    //Create editor to allow changes.
    SharedPreferences.Editor editor;
    //parking duration.
    Long parkingTime;
    //cost of parking.
    Long parkingFee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_aid_info);

        //Initialise objects.
        parkingInfo = PreferenceManager.getDefaultSharedPreferences(this);
        editor = parkingInfo.edit();

        //Handle add info button press
        findViewById(R.id.btnInfo).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                EditText editTime = (EditText)findViewById(R.id.textTime);
                EditText editCost = (EditText)findViewById(R.id.textCost);


                if(editTime.getText().toString().trim().length() > 0 && editCost.getText().toString().trim().length() > 0
                        && Integer.parseInt(editTime.getText().toString()) % 1 == 0) {
                    parkingTime = Long.valueOf(editTime.getText().toString());
                    parkingFee = Long.valueOf(editCost.getText().toString());

                    //Testing the output.
                    Log.d("Value: ", Long.toString(parkingTime));
                    Log.d("Value", String.valueOf(parkingFee));

                    //Store the users parking information.
                    editor.putLong("parkingTime", parkingTime);
                    editor.putLong("parkingFee", parkingFee);
                    //commits the parking information so its stored.;
                    editor.commit();

                    Intent intent = new Intent(ParkingAidInfoActivity.this, ParkingAidMainActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "You must enter your details!",
                            Toast.LENGTH_SHORT).show();

                }
            }
        });

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }
}
