package uk.ac.uea.guidedtours;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import uk.ac.uea.Framework.POI;

/**
 * Created by xbv13gyu on 18/01/2016.
 */
public class GuidedToursQActivity extends AppCompatActivity {
    ListView list_view; // List used to display a list of answers to a question
    TextView textView;  // Text used to display a question
    Button button; // Button to go to next question
    ArrayAdapter<String> adapter;
    ArrayList<String[]> quiz; //ArrayList used to store a number of lists of answers
    ArrayList<Integer> quizTitle; //ArrayList used to store a number of questions
    ArrayList<POI> PoiList; //ArrayList used to store a number of POI's
    String selection; //The answer the user chooses
    ArrayList<String> list_items;
    POI poi; //a poi object to be stored and passed to mainActivty
    int i; //position in quiz

    /**
     * In this method the quiz is created, this method then checks whether answers are selected or
     * a user wants to go to the next question/mainActivity
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guided_tours_q);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        quiz = new ArrayList<>();
        quizTitle = new ArrayList<>();
        PoiList = new ArrayList<>();
        list_items = new ArrayList<>();
        i = 0;
        buildQuiz();

        list_view = (ListView) findViewById(R.id.list_view);
        nextQuestion();
        /**
         * Below the list_view is checked for clicks, when an object inside the list is clicked
         * it's added to list_items which stores a list of places the user wishes to visit
         */
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if( parent.getAdapter().getView(position, view, parent).isEnabled()) {
                    String string = (String) parent.getAdapter().getItem(position);
                    list_items.add(string);
                    parent.getAdapter().getView(position, view, parent).setEnabled(false);
                    Toast.makeText(GuidedToursQActivity.this, string, Toast.LENGTH_SHORT).show();
                    Log.d("My msg: ", "LIST SIZE:" + list_items.size());
                }
                else
                    Toast.makeText(GuidedToursQActivity.this, "Already chosen", Toast.LENGTH_SHORT).show();
            }
        });
        /**
         * When user clicks to go next this checks whether to show another page of questions or to
         * send the user to mainActivity by the sendLocation method
         */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                if (i > (quiz.size() - 1)) {
                    sendLocations(list_items);
                }
                else if (i <= (quiz.size() - 1)) {
                    nextQuestion();
                }
            }
        });
    }
    /**
     * Method used to turn get the locations from a number of selected Strings, it sends these
     * locations to the MainActivity
     * @param listOfItems a number of selected Strings from the quiz
     */
    public void sendLocations(ArrayList<String> listOfItems){
        for(int v = 0; v< listOfItems.size(); v++)
        {
         poi = null;
            selection = listOfItems.get(v);
                switch (selection) {
                    case "CMP":
                        poi = new POI(52.621051, 1.237687, "CMP", "Computing department");
                        break;
                    case "MTH":
                        poi = new POI(52.62101, 1.23769, "MTH", "Math department");
                        break;
                    case "BIO":
                        poi = new POI(52.620592, 1.236437, "BIO", "Biology department");
                        break;
                    case "CHEM":
                        poi = new POI(52.621485, 1.239243, "CHEM", "Chemistry department");
                        break;
                    case "PHARM":
                        poi = new POI(52.621486, 1.239243, "PHARM", "Pharmacy department");
                        break;
                    case "HIS":
                        poi = new POI(52.62211, 1.24053, "HIS", "History department");
                        break;
                    case "BUS":
                        poi = new POI(52.621841, 1.236162, "BUS", "Norwich Business School");
                        break;
                    case "ECO":
                        poi = new POI(52.622316, 1.240763, "ECO", "School of Economics");
                        break;
                    case "ENV":
                        poi = new POI(52.621428, 1.238282, "ENV", "Environmental Science department");
                        break;
                    case "PSY":
                        poi = new POI(52.621533, 1.237184, "PSY", "Psycology department");
                        break;
                    case "Sports":
                        poi = new POI(52.624256, 1.240652, "Sports", "Sports park");
                        break;
                    case "Reading":
                        poi = new POI(52.621107, 1.240663, "Library", "The library");
                        break;
                    case "Arts":
                        poi = new POI(52.620694, 1.234602, "The Sainsbury Centre", "Centre for the Visual Arts");
                        break;
                    case "Coffee":
                        poi = new POI(52.621102, 1.241658, "Cafe", "Cafe direct");
                        break;
                    case "Lake":
                        poi = new POI(52.619050, 1.242911, "Lake", "The Lake");
                        break;
                    case "Pub":
                        poi = new POI(52.621577, 1.241040, "Student union pub and bar", "pub");
                        break;
                    case "City centre":
                        poi = new POI(52.625018, 1.289761, "City centre", "City centre");
                        break;
                    case "Paston House":
                        poi = new POI(52.622589, 1.245017, "Paston House", "University of East Anglia");
                        break;
                    case "University Village":
                        poi = new POI(52.628215, 1.237672, "University Village", "University of East Anglia");
                        break;
                    case "Suffolk Terrace":
                        poi = new POI(52.620174, 1.240125, "Suffolk Terrace", "University of East Anglia");
                        break;
                    case "Nelson Court":
                        poi = new POI(52.621319, 1.243914, "Nelson Court", "University of East Anglia");
                        break;
                    case "Britten House":
                        poi = new POI(52.622128, 1.244792, "Britten House", "University of East Anglia");
                        break;
                    case "Constable Terrace":
                        poi = new POI(52.621407, 1.234353, "Constable Terrace", "University of East Anglia");
                        break;
                    case "Family Accomodation":
                        poi = new POI(52.620365, 1.241316, "Family Accomodation", "University of East Anglia");
                        break;
                    case "Victory House":
                        poi = new POI(52.622639, 1.24638, "Victory House", "University of East Anglia");
                        break;
                }
                PoiList.add(poi);
        }
                    Intent intent = new Intent(GuidedToursQActivity.this, GuidedToursMainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("POI", PoiList);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    startActivity(intent);
                }

    /**
     * Method used to build the quiz
     * Builds a list of questions and a list of corresponding answers.
     * Also initialises button and text view
     */
    public void buildQuiz() {
        quiz.add(getResources().getStringArray(R.array.Which_school));
        quiz.add(getResources().getStringArray(R.array.What_to_visit));
        quiz.add(getResources().getStringArray(R.array.Main_hobbey));
        quiz.add(getResources().getStringArray(R.array.What_accomodation_to_visit));
        quizTitle.add(R.string.select_school);
        quizTitle.add(R.string.select_visit);
        quizTitle.add(R.string.select_hobby);
        quizTitle.add(R.string.select_accomodation);
        button = (Button) findViewById(R.id.btnContinue);
        textView = (TextView) findViewById(R.id.textViewID);
    }


    /**
     * Method to go to the next question.
     * Method gets the next question and the next list of answers (position i)
     */
    public void nextQuestion() {
        textView.setText(quizTitle.get(i));
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, quiz.get(i));
        list_view.setAdapter(adapter);
        list_view.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

    }



}
