package uk.ac.uea.guidedtours;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.uea.Framework.AndroidLocation;
import uk.ac.uea.Framework.EndActivity;
import uk.ac.uea.Framework.POI;


public class GuidedToursMainActivity extends uk.ac.uea.Framework.MainActivity {

    ArrayList<AndroidLocation> poiList;
    Parcelable[] answers;
    Boolean setUp = false;
    AndroidLocation nextLocation;
    AndroidLocation endLocation;
    Button acceptRoute, skipLocation;
    int targetLocationIndex = -1;
    TextView txtDescription;
    Button btnNext;
    boolean startedTour;
    boolean oneLocation;

    /**
     * Method to start the MainActivity, adds map to screen checks user is OK with route then displays
     * next location. Once the user reaches the location or wishes to skip the next location will be
     * displayed
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guided_tours_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        answers = new Parcelable[3];
        poiList = getIntent().getParcelableArrayListExtra("POI");
        startedTour = false;
        txtDescription = (TextView) findViewById(R.id.txtHint);
        btnNext = (Button) findViewById(R.id.btnNext);
        acceptRoute = (Button) findViewById(R.id.btnAccept);
        skipLocation = (Button) findViewById(R.id.btnSkip);
        skipLocation.setEnabled(false);
        skipLocation.setVisibility(View.INVISIBLE);
        addMapFragment();

        if (poiList.isEmpty()) {
            acceptRoute.setVisibility(View.INVISIBLE);
            txtDescription.setText("Tour is over");
            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
            final TextView input = new TextView(this);
            alert.setTitle("No locations entered- tour over");
            input.setText("We have detected that you have not entered any locations. Thank for you using our app!");
            alert.setView(input);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(GuidedToursMainActivity.this, EndActivity.class);
                    startActivity(intent);
                }
            });

            alert.show();
        } else {
            acceptRoute.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    startLocationUpdates();
                    startedTour = true;
                    nextTarget();
                    acceptRoute.setVisibility(View.INVISIBLE);
                    skipLocation.setEnabled(true);
                    skipLocation.setVisibility(View.VISIBLE);
                }
            });

            skipLocation.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    nextTarget();
                }
            });

        }
    }

    /**
     * Method for when the users location has changed. Updates map and checks if user is at the
     * desired location
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);

        if (map != null && !setUp && !poiList.isEmpty()) {

            drawCompleteRoute();
            setUp = true;
        }

        if (startedTour)
            if (map.isUserAtLocation(nextLocation)) {
                stopLocationUpdates();
                btnNext.setText("Next Location");
                btnNext.setVisibility(View.VISIBLE);

                //Handle next hint button
                findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        btnNext.setVisibility(View.INVISIBLE);
                        startLocationUpdates();
                        nextTarget();
                    }
                });
            }
    }

    /**
     * Method used to draw the route on the map
     */

    public void drawCompleteRoute() {
        stopLocationUpdates();
        mapUpdates = false;

        for (AndroidLocation loc : poiList) {
            map.addMarker(loc, ((POI) loc).getName(), ((POI) loc).getDescription());
        }
        endLocation = poiList.get(poiList.size() - 1);
        //change when in Norwich
        map.moveCamera(currentAndroidLocation, 15);

        map.routeTo(endLocation, poiList, "walking");

        if(poiList.size() > 1)
            poiList.remove(poiList.size() - 1);
        else
            oneLocation = true;

        txtDescription.setText("Above is your route. Please press start to begin the tour.");


    }

    /**
     * Method used for the next location to be displayed- if the list of locations is empty
     * the user will be displayed an appropriate page
     */
    private void nextTarget() {

        if (!poiList.isEmpty()) {
            map.clear();
            nextLocation = map.findClosestLocation(poiList);
            map.moveCamera(currentAndroidLocation);
            map.addMarker(nextLocation, ((POI) nextLocation).getName(), ((POI) nextLocation).getDescription());
            txtDescription.setText("Your next location is " + ((POI) nextLocation).getName() + ". " + ((POI) nextLocation).getDescription());
            map.routeTo(nextLocation, "walking");

            poiList.remove(nextLocation);

        }
        else {
            txtDescription.setText("Tour is over");
            stopLocationUpdates();
            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
            final TextView input = new TextView(this);
            alert.setTitle("You have finished the Tour");
            input.setText("We have detected that you have finished the tour. Thank for you using our app!");
            alert.setView(input);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(GuidedToursMainActivity.this, EndActivity.class);
                    startActivity(intent);
                }
            });

            alert.show();
        }
    }

}
