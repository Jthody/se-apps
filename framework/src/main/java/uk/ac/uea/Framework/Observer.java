package uk.ac.uea.Framework;

/**
 * Abstract class used to update the information of an Observer object.
 */
public abstract class Observer {

    public abstract void Update();
}
