package uk.ac.uea.Framework;

/**
 * This class is used as an example to show what a class worth saving should contain, using the
 * Memento pattern.
 */
public class Originator {

    protected MementoState state;

    /**
     * Used to set the state of an Android Location so it can be retrieved.
     * @param state
     */
    public final void setState(MementoState state) { this.state = state; }

    /**
     * Method thats used to save the state to the Memento Class
     * @return a new Memento state.
     */
    public final Memento saveToMemento() { return new Memento(state); }

    /**
     * Method used to retrieve the saved state.
     * @param m
     */
    public final void restoreFromMemento(Memento m) { state = m.getState(); }
}
