package uk.ac.uea.Framework;

/**
 * A class used to create an HTML strategy using the the FormatStrategy class.
 */
public class HTMLStrategy extends FormatStrategy {

    /**
     * Method used to set the format of the HTML output.
     * @param Heading
     * @param Body
     * @return
     */
    @Override
    public String Format(String Heading, String Body) {
        return "<HTML>\n<HEAD>\n<TITLE>" + Heading + "</TITLE>\n\n<BODY>\n" + Body + "\n</BODY>\n</HTML>";
    }
}
