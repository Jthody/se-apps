package uk.ac.uea.Framework;

import android.app.Activity;

/**
 * Created by michaelcoulson on 16/01/16.
 */
public class HTMLExternalFactory extends AbstractStorageFactory {
    public HTMLExternalFactory(Activity TheAct) {
        super(TheAct);
    }

    @Override
    public FormatStrategy getStrategy() {
        return new HTMLStrategy();
    }

    @Override
    public FileImplementation getFileImp() {
        return new ExternalFileImp(Act);
    }
}

