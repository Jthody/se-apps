package uk.ac.uea.Framework;

import android.app.Activity;

/**
 * An abstract class used to implement reading and writing to files.
 */
public abstract class FileImplementation {

    protected Activity Act;

    /**
     * Constructor for the FileImplementation used to set Act.
     * @param TheAct
     */
    protected FileImplementation(Activity TheAct) {
        Act = TheAct;
    }

    /**
     * Abstract Method used to read in a file.
     * @param FileName
     * @return
     */
    public abstract String read(String FileName);

    /**
     * Abstract method used to write content to a file.
     * @param FileName
     * @param Contents
     * @return
     */
    public abstract boolean write(String FileName, String Contents);

}
