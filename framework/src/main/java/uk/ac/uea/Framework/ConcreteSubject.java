package uk.ac.uea.Framework;

/**
 * Class that implements ConcreteSubject in which a state of a subject can take on many different
 * forms. Generics are used to handle each type.
 * @param <T>
 */
public final class ConcreteSubject<T> extends Subject {

    private T State;

    /**
     * Method used to return the state of an object.
     * @return state
     */
    public T GetState() {
        return State;
    }

    /**
     * Methdod used to set the state of an object.
     * @param newState
     */
    public void SetState(T newState) {
        State = newState;
        this.Notify();
    }
}
