package uk.ac.uea.Framework;

/**
 * class used as the mechanism for generic formatting of the output of text.
 */
public class SimpleText extends AbstractFile{

    String Heading = "";
    String Body = "";
    private FormatStrategy MyStrat = new StringStrategy();

    /**
     * Method used to create a new FileImplementation of a SimpleText object,
     * @param Imp
     */
    protected SimpleText(FileImplementation Imp) {
        super(Imp);
    }

    /**
     * Method that obtains a stored formating strateg.
     * @param Fact
     */
    public SimpleText(AbstractStorageFactory Fact) {
        super(Fact.getFileImp());
        MyStrat = Fact.getStrategy();
    }

    /**
     * Method used to set the heading of the text.
     * @param heading
     */
    public void setHeading(String heading) {
        Heading = heading;
    }

    /**
     * Method used to set the body of the text.
     * @param body
     */
    public void setBody(String body) {
        Body = body;
    }

    /**
     * Method that returns the format of the simpleText object.
     * @return
     */
    public String FormatOutput() {
        return MyStrat.Format(Heading, Body);
    }

    /**
     * Method that initialises the current format strategy for to be used to format the output.
     * @param myStrat
     */
    public void setMyStrat(FormatStrategy myStrat) {
        MyStrat = myStrat;
    }

    /**
     * Method that is used to write to file.
     * @param FileName
     * @return
     */
    public boolean writeToFile(String FileName) {
        return write(FileName, FormatOutput());
    }

    /**
     * Method that is used to read contents from a file.
     * @param FileName
     * @return
     */
    public boolean readFromFile(String FileName) {
        String FileContents = read(FileName);
        Heading = "";
        Body = FileContents;
        return !FileContents.contains("ERROR_");
    }
}
