package uk.ac.uea.Framework;

/**
 * This interface can be implemented to make an instance of a platform specific sound, this sound
 * can then be played and disposed.
 */
public interface Sound {

    /**
     * This method will play a sound at a set volume.
     * @param volume the volume level at which the sound will be played.
     */
    public void play(float volume);

    /**
     * This method can be implemented to stop a sound from playing.
     */
    public void stop();

    /**
     * This method can be implemented to remove a sound from memory.
     */
    public void dispose();
}
