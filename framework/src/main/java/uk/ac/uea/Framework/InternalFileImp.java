package uk.ac.uea.Framework;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * class used to implement internal storage used for the Bridge design pattern.
 */
public class InternalFileImp extends FileImplementation {

    /**
     * Constructor for the InternalFileImp.
     * @param TheAct
     */
    public InternalFileImp(Activity TheAct) {
        super(TheAct);
    }

    /**
     * Method for reading to internal storage.
     * @param FileName
     * @return
     */
    @Override
    public String read(String FileName) {
        FileInputStream fis;
        try {
            fis = Act.openFileInput(FileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "ERROR_FILE_NOT_FOUND";
        }

        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR_WHILE_READING_FILE";
        }
    }

    /**
     * Method used to write to internal Storage.
     * @param FileName
     * @param Contents
     * @return
     */
    @Override
    public boolean write(String FileName, String Contents) {
        FileOutputStream fos;
        try {
            fos = Act.openFileOutput(FileName, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        try {
            fos.write(Contents.getBytes());
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
