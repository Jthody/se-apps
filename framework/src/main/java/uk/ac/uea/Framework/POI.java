package uk.ac.uea.Framework;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Max on 22-Dec-15.
 */
public class POI extends AndroidLocation implements Parcelable {

    protected String name;
    protected String description;

    /**
     * Constructor for a POI object
     * @param latitude
     * @param longitude
     * @param name Name of the Point of interest
     * @param description  Description of the point of interest
     */
    public POI(double latitude, double longitude, String name, String description) {
        super(latitude, longitude);
        this.name = name;
        this.description = description;
    }

    /**
     * Get the description
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the name of the POI
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the POI
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the description of the POI
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "POI{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    protected POI(Parcel in) {
        state = (AndroidLocation) in.readValue(AndroidLocation.class.getClassLoader());
        m_Loc = (Location) in.readValue(Location.class.getClassLoader());
        name = in.readString();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(state);
        dest.writeValue(m_Loc);
        dest.writeString(name);
        dest.writeString(description);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<POI> CREATOR = new Parcelable.Creator<POI>() {
        @Override
        public POI createFromParcel(Parcel in) {
            return new POI(in);
        }

        @Override
        public POI[] newArray(int size) {
            return new POI[size];
        }
    };
}