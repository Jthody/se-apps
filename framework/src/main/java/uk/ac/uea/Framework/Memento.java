package uk.ac.uea.Framework;

/**
 * This class wraps the actual state of a program in a manageable form. Used to store and return a
 * MementoState class
 */
public class Memento {

    private MementoState state;

    /**
     * Constructor for Memento used to set the state.
     * @param newState
     */
    public Memento(MementoState newState) {
        state = newState;
    }

    /**
     * Method that is used to retrieve a memento state
     * @return the MementoState state.
     */
    public MementoState getState() {
        return state;
    }
}
