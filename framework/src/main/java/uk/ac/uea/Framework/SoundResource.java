package uk.ac.uea.Framework;

import android.app.Activity;

/**
 * Class designed to load, play and stop a sounds resource and
 */
public class SoundResource {

    private Audio MyAudio;
    private Sound MySound;

    /**
     * Constructor for SoundResource that sets MyAudio.
     * @param Act
     */
    public SoundResource(Activity Act){
        MyAudio = new AndroidAudio(Act);

    }

    /**
     * Method used to load a sound from a requested path and store it in MySound.
     */
    public void load(String resourcePath){
        MySound = MyAudio.createSound((resourcePath));

    }

    /**
     * Method used to play the sounds stored in MySound at a preset volume.
     */
    public void play(){
        MySound.play((float)0.9);//refactor the volume parameter for final work.

    }

    /**
     * Method used to stop the sound from playing.
     */
    public void stop(){
        MySound.stop();

    }
}
