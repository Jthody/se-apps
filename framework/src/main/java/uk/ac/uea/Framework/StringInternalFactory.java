package uk.ac.uea.Framework;

import android.app.Activity;

/**
 * Class for setting a string strategy from and existing source.
 */
public class StringInternalFactory extends AbstractStorageFactory {

    public StringInternalFactory(Activity TheAct) {
        super(TheAct);
    }

    /**
     * Method used to return FormatStrategy
     * @return
     */
    @Override
    public FormatStrategy getStrategy() {
        return new StringStrategy();
    }

    /**
     * Method that is used to retrieve a FileImplementation.
     * @return
     */
    @Override
    public FileImplementation getFileImp() {
        return new InternalFileImp(Act);
    }
}
