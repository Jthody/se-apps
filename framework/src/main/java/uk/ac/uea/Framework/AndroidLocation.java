package uk.ac.uea.Framework;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * This class provides basic Location functionality using the Android Location class.
 * Created by joshthody on 15/12/2015.
 */
public class AndroidLocation extends MementoState implements Parcelable {

    protected AndroidLocation state;

    //Android Location object
    Location m_Loc;

    /**
     *
     Retrieve the state of an AndroidLocation.
     */
    public AndroidLocation getState() { return state; }

    /**
     * Set the state of AndroidLocation.
     * @param state
     */
    public void setState(AndroidLocation state) { this.state = state;}


    /**
     * Get the bearing of the Location
     */
    public float getBearing() {
        return m_Loc.getBearing();
    }

    /**
     * Set the bearing of the location to the given value
     * @param bearing
     */
    public void setBearing(float bearing) {
        m_Loc.setBearing(bearing);
    }

    /**
     * Get the altitude of the Location
     * @return altitude
     */
    public double getAltitude() {
        return m_Loc.getBearing();
    }

    /**
     * Set the altitude of the Location
     * @param altitude
     */
    public void setAltitude(double altitude) {
        m_Loc.setAltitude(altitude);
    }


    /**
     * Default constructor.
     * Sets Longitude and Latitude to 0
     * Sets Altitude and Bearing to 0
     */
    public AndroidLocation()
    {
        m_Loc = new Location("");
        m_Loc.setAltitude(0);
        m_Loc.setLatitude(0);
        m_Loc.setLongitude(0);
        m_Loc.setBearing(0);
    }

    /**
     * Constructor that takes a value for the longitude and latitude.
     * Sets bearing and altitude to 0
     * @param latitude
     * @param longitude
     */
    public AndroidLocation(double latitude, double longitude)
    {
        m_Loc = new Location("");
        m_Loc.setAltitude(0);
        m_Loc.setLatitude(latitude);
        m_Loc.setLongitude(longitude);
        m_Loc.setBearing(0);
    }

    /**
     * Constructor that assigns a value to all member variables
     * @param latitude
     * @param longitude
     * @param altitude
     * @param bearing
     */
    public AndroidLocation(double latitude, double longitude, double altitude, float bearing)
    {
        m_Loc = new Location("");
        m_Loc.setAltitude(altitude);
        m_Loc.setLatitude(latitude);
        m_Loc.setLongitude(longitude);
        m_Loc.setBearing(bearing);
    }

    /**
     * Constructor that takes a Location object
     * @param l
     */
    public AndroidLocation(Location l)
    {
        m_Loc = l;
    }

    /**
     * Constructor that takes an AndroidLocation object
     * @param l
     */
    public AndroidLocation(AndroidLocation l)
    {
        m_Loc = l.m_Loc;
    }

    /**
     * Set the latitude of the location
     * @param l
     */
    public void setLatitude(double l)
    {
        m_Loc.setLatitude(l);
    }
    /**
     * Set the longitude of the location
     * @param l
     */
    public void setLongitude(double l)
    {
        m_Loc.setLongitude(l);;
    }

    /**
     * Get the latitude
     * @return
     */
    public double getLatitude()
    {
        return m_Loc.getLatitude();
    }

    /**
     * Get the longitude
     * @return
     */
    public double getLongitude()
    {
        return m_Loc.getLongitude();
    }

    /**
     * Get the being between this location and the passed Location object
     * @param loc Location object
     * @return value of the bearing between the two locations
     */
    private float bearingTo(Location loc)
    {

        return m_Loc.bearingTo(loc);

    }

    /**
     * Get the being between this location and the passed AndroidLocation object
     * @param loc AndroidLocation object
     * @return value of the bearing between the two locations
     */
    public float bearingTo(AndroidLocation loc)
    {

        return bearingTo(loc.m_Loc);

    }

    /**
     * Update Location
     * @param l
     */
    public void update(Location l)
    {
        m_Loc = l;
    }

    /**
     * Update Location
     * @param l
     */
    public void update(AndroidLocation l)
    {
        m_Loc = l.m_Loc;
    }


    @Override
    public String toString() {
        return "AndroidLocation{" +
                "latitude=" + m_Loc.getLatitude() +
                ", longitude=" + m_Loc.getLongitude() +
                '}';
    }

    protected AndroidLocation(Parcel in) {
        state = (AndroidLocation) in.readValue(AndroidLocation.class.getClassLoader());
        m_Loc = (Location) in.readValue(Location.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(state);
        dest.writeValue(m_Loc);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AndroidLocation> CREATOR = new Parcelable.Creator<AndroidLocation>() {
        @Override
        public AndroidLocation createFromParcel(Parcel in) {
            return new AndroidLocation(in);
        }

        @Override
        public AndroidLocation[] newArray(int size) {
            return new AndroidLocation[size];
        }
    };
}