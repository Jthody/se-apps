package uk.ac.uea.Framework;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class EndActivity extends  Activity {
    Button parking;
    Button guidedtour;
    Button treasurehunt;
    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_end);
        parking = (Button) findViewById(R.id.parkanother);
        guidedtour = (Button) findViewById(R.id.guidedtour);
        treasurehunt = (Button) findViewById(R.id.treasure);


        ImageView iv = (ImageView)findViewById(R.id.logo);
        iv.setImageResource(R.drawable.uealogo2);


    }


    public void parkingaidload(View v){

        Log.e("error", "h");
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage("uk.ac.uea.parkingaid");
        startActivity(intent);


    }


    public void guidedtourload(View v){

        Log.e("error", "h");
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage("uk.ac.uea.guidedtours");
        startActivity(intent);


    }
    public void treasurehuntload(View v){

        Log.e("error", "h");
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage("uk.ac.uea.treasurehunt");
        startActivity(intent);


    }

        }



