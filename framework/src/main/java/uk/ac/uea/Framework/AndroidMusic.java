package uk.ac.uea.Framework;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;


/**
 * This class is an implementation of the Music interface to create an instance of a peice of
 * platform specific music
 */
public class AndroidMusic implements Music, OnCompletionListener, OnSeekCompleteListener,
        OnPreparedListener, OnVideoSizeChangedListener {

    MediaPlayer mediaPlayer;
    boolean isPrepared = false;
    private static AndroidMusic instance;

    /**
     * Constructor for the AndroidMusic
     * @param assetDescriptor file descriptor of an entry in the AssetManager
     */
    private AndroidMusic(AssetFileDescriptor assetDescriptor) {

        load(assetDescriptor);
    }

    /**
     * Method used to load an asset file.
     * @param assetDescriptor
     */
    private void load(AssetFileDescriptor assetDescriptor){

        mediaPlayer = new MediaPlayer();
        try{

            mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor());
            assetDescriptor.getStartOffset();
            assetDescriptor.getLength();
            mediaPlayer.prepare();
            isPrepared = true;
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music");
        }
    }

    /**
     * Method used to return an instance of an asset.
     * @param assetDescriptor
     * @return
     */
    public static AndroidMusic getInstance(AssetFileDescriptor assetDescriptor){
        if(instance == null){
            instance = new AndroidMusic(assetDescriptor);
        }
        else{
            instance.stop();
            instance.load(assetDescriptor);
        }
        return instance;
    }

    /**
     * Method overriden from the Music framework- used to remove a music object from memory
     */
    @Override
    public void dispose() {
    
         if (this.mediaPlayer.isPlaying()){
               this.mediaPlayer.stop();
                }
        this.mediaPlayer.release();
    }

    /**
     * Method overriden from the Music framework- used to return true if the music is looping- false
     * otherwise
     * @return
     */
    @Override
    public boolean isLooping() {
        return mediaPlayer.isLooping();
    }

    /**
     * Method overriden from the Music framework- used to return true if the music is playing- false
     * otherwise
     * @return
     */
    @Override
    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    /**
     * Method overriden from the Music framework- used to return true if the music is stopped- false
     * otherwise
     * @return
     */
    @Override
    public boolean isStopped() {
        return !isPrepared;
    }

    /**
     * Method overriden from the Music framework- used to pause the music
     */
    @Override
    public void pause() {
        if (this.mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }


    /**
     * Method overriden from the Music framework- used to play the music
     */
    @Override
    public void play() {
        if (this.mediaPlayer.isPlaying())
            return;

        try {
            synchronized (this) {
                if (!isPrepared)
                    mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method overriden from the Music framework- used to set music looping on/off
     * @param isLooping This boolean is used to decide whether the will loop
     */
    @Override
    public void setLooping(boolean isLooping) {
        mediaPlayer.setLooping(isLooping);
    }

    /**
     * Method overriden from the Music framework- used to set the volume of the music
     * @param volume a level to which to set the volume to
     */
    @Override
    public void setVolume(float volume) {
        mediaPlayer.setVolume(volume, volume);
    }

    /**
     * Method overriden from the Music framework- used to stop the music
     */
    @Override
    public void stop() {
         if (this.mediaPlayer.isPlaying() == true){
        this.mediaPlayer.stop();
        
       synchronized (this) {
           isPrepared = false;
        }}
    }

    /**
     * Method overriden from the MediaPlayer class- states what to do when the music is finished
     * @param player The passed MediaPlayer
     */
    @Override
    public void onCompletion(MediaPlayer player) {
        synchronized (this) {
            isPrepared = false;
        }
    }

    /**
     * Method overriden from Music framework- used to find the beginning of the music
     */
    @Override
    public void seekBegin() {
        mediaPlayer.seekTo(0);
        
    }

    /**
     * Method to set isPrepared to true
     * @param player the passed MediaPlayer
     */
    @Override
    public void onPrepared(MediaPlayer player) {
        // TODO Auto-generated method stub
         synchronized (this) {
               isPrepared = true;
            }
        
    }

    /**
     * Method not implemented
     * @param player the passed MediaPlayer
     */
    @Override
    public void onSeekComplete(MediaPlayer player) {
        // TODO Auto-generated method stub
        
    }

    /**
     * Method not implemented
     * @param player the passed MediaPlayer
     * @param width Width of the video
     * @param height Height of the video
     */
    @Override
    public void onVideoSizeChanged(MediaPlayer player, int width, int height) {
        // TODO Auto-generated method stub
        
    }
}
