package uk.ac.uea.Framework;

/**
 * This interface can be implemented to make an instance of a subject state.
 * @param <T>
 */
public interface ObserverClient<T> {

    public void Update(T StateUpdate);
}