package uk.ac.uea.Framework;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * This is the MainActivity class which is extended by all of our apps. It handles Location and hardware sensor changes changes
 */
public abstract class MainActivity extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SensorEventListener  {

    protected AndroidMap map;
    protected AndroidLocation currentAndroidLocation;
    protected LocationRequest mLocationRequest;
    private GoogleApiClient client;
    protected MapFragment fragment;
    protected boolean mapUpdates;
    protected boolean compassEnabled;
    protected Compass compass;
    private SensorManager mSensorManager;
    private Sensor sensor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Create default Location object (0,0)
        currentAndroidLocation = new AndroidLocation();

        //Set up location requests
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        client.connect();
        createLocationRequest();

        //For the splash loading screen
        //setContentView(R.layout.activity_main);

        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        if (sensor != null) {
           mSensorManager.registerListener(this, sensor,
                    SensorManager.SENSOR_DELAY_GAME);
        }

        //enable map update flag
        mapUpdates = true;
        //compass flag false by default
        compassEnabled = false;
        //map initially set to null as it needs to be set up
        map = null;
    }


    /**
     * Method that gets called if the phone detects there is a change in the sensor
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        //If the compass is enabled tell it there has been a sensor change
        if(compassEnabled)
            compass.onSensorChanged(event);
    }

    /**
     * Method that gets called if the phone detects there has been an accuracy change in the sensor
     * @param sensor
     * @param accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if(compassEnabled)
            compass.onAccuracyChanged(sensor, accuracy);
    }


    /**
     * Method that creates the location requests
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Method that adds the map fragment to the screen
     */
    protected void addMapFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        fragment = new MapFragment();
        transaction.add(R.id.mapView, fragment);
        transaction.commit();
    }

    /**
     * Method that hides or shows a fragment
     * @param fragment fragment to hide/show
     */
    public void showHideFragment(final Fragment fragment) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (fragment.isHidden()) {
            ft.show(fragment);

        } else {
            ft.hide(fragment);

        }

        ft.commit();
    }

    @Override
    public void onConnected(Bundle bundle) {

        startLocationUpdates();
    }

    /**
     * Stop location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                client, this);
    }

    /**
     * Start location updates
     */
    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                client, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Method that is called when the phone detects there is a Location change
     * @param location Users new Location
     */
    @Override
    public void onLocationChanged(Location location) {

        currentAndroidLocation.update(location);

        if (AndroidMap.hasInstance && map == null) {
            map = AndroidMap.getMap();
            map.updateLocation(currentAndroidLocation);
            map.moveCamera(currentAndroidLocation);
        } else  {
            map.updateLocation(currentAndroidLocation);
            if(mapUpdates)
                map.update();

            if(compassEnabled)
                compass.updateUserLoc(currentAndroidLocation);

        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settings) {
            Intent intent1 = new Intent(this, SettingsActivity.class);
            this.startActivity(intent1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);


    }

    @Override
    protected void onPause() {
        super.onPause();

        // to stop the listener and save battery
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
    }




}
