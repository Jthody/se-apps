package uk.ac.uea.Framework;

import java.util.ArrayList;

/**
 * This class is a container class for the Memento class instances.
 */
public class CareTaker {

    //initilaising arraylist
    private ArrayList<Memento> savedStates = new ArrayList<Memento>();

    /**
     * Method used to save a Memento object to the saved state arraylist.
     * @param m
     */
    public void addMemento(Memento m) {
        savedStates.add(m);
    }

    /**
     * Method used to retrieve a state at a given position in the arraylist.
     * @param index
     * @return
     */
    public Memento getMemento(int index) {
        return savedStates.get(index);
    }
}
