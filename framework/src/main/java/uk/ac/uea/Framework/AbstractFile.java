package uk.ac.uea.Framework;

/**
 * Abstract class used to read and write content of a file.
 */
public abstract class AbstractFile {

    private FileImplementation FileImp;

    /**
     * Constructor for the AbstractFile used to set FileImp.
     * @param Imp
     */
    protected AbstractFile(FileImplementation Imp) {
        FileImp = Imp;
    }

    /**
     * Method used to read a filename.
     * @param FileName
     * @return
     */
    protected final String read(String FileName) {
        return FileImp.read(FileName);
    }

    /**
     * Method used to write content to a file.
     * @param FileName
     * @param Contents
     * @return
     */
    protected final boolean write(String FileName, String Contents) {
        return FileImp.write(FileName, Contents);
    }

    /**
     *  Method used to initialise a new FileImplementation.
     * @param NewImp
     */
    public void setImplementation(FileImplementation NewImp) {
        FileImp = NewImp;
    }
}
