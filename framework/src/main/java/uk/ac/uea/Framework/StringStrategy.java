package uk.ac.uea.Framework;

/**
 * Class that is used to create a strategy for formatting strings by extending the formatStrategy
 */
public class StringStrategy extends FormatStrategy {

    /**
     * MEthod used to format the output of String text.
     * @param Heading
     * @param Body
     * @return
     */
    @Override
    public String Format(String Heading, String Body) {
        return Heading + "\n\n" + Body; }
}
