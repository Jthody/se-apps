package uk.ac.uea.Framework;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Observer;

/**
 * The Subject class uses the Observer pattern to create a generic mechanism in the framework. You
 * can use this mechanism to create new subjects and observers by inheriting from the base Observer
 * Pattern structure
 */
public abstract class Subject {

    //Initialise the hashset
    private HashSet<Observer> observers = new HashSet<Observer>();

    /**
     * Method used to insert an Observer object from the hashSet.
     * @param obs
     */
    public final void Attach(Observer obs) {
        if (!observers.contains(obs)) {
            observers.add(obs);
        }
    }

    /**
     * Method used to remove an Observer object from the hashSet.
     * @param obs
     */
    public final void Detach(Observer obs) {
        if (observers.contains(obs)) {
            observers.remove(obs);
        }
    }

    /**
     * Method used to notify by iterating through the hashset.
     */
    protected final void Notify() {
        Iterator<Observer> ObIt = observers.iterator();
        while (ObIt.hasNext()) {
            //ObIt.next().Update();
        }
    }
}
