package uk.ac.uea.Framework;

/**
 * This interface can be implemented to make an instance of a platform specific music
 */
public interface Music {

    /**
     *This method can be implemented to play the music
     */
    public void play();

    /**
     *This method can be implemented to stop the music
     */
    public void stop();

    /**
     *This method can be implemented to pause the music
     */
    public void pause();

    /**
     *This method can be implemented to set music looping on/off
     * @param looping- This boolean is used to decide whether the music will loop
     */
    public void setLooping(boolean looping);

    /**
     * This method can be implemented to set the volume of the music
     * @param volume a level to which to set the volume to
     */
    public void setVolume(float volume);

    /**
     *This method can be implemented to return true if the music is playing- false otherwise
     */
    public boolean isPlaying();

    /**
     *This method can be implemented to return true if the music is stopped- false otherwise
     */
    public boolean isStopped();

    /**
     *This method can be implemented to return true if the music is looping- false otherwise
     */
    public boolean isLooping();

    /**
     * * This method can be implemented to remove a music from memory
     */
    public void dispose();

    /**
     * * This method can be implemented to find the beggining of the music
     */
    void seekBegin();
}