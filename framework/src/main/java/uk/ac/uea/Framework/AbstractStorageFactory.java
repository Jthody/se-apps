package uk.ac.uea.Framework;

import android.app.Activity;

/**
 * Abstract class used to obtain stored strategies for an activity.
 */
public abstract class AbstractStorageFactory {

    protected Activity Act;

    /**
     * Constructor for AbstractStorageFactory that sets Act.
     * @param TheAct
     */
    public AbstractStorageFactory(Activity TheAct) {
        Act = TheAct;
    }

    /**
     * method used to obtain a formatStrategy
     * @return
     */
    public abstract FormatStrategy getStrategy();

    /**
     * Method used to retrieve a FileImplementation.
     * @return
     */
    public abstract FileImplementation getFileImp();
}
