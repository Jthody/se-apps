package uk.ac.uea.Framework;

/**
 * Class used in connection with the memento strtegy to save the state of an Android Location.
 */
public class AndroidLocationOriginator extends Originator {

    /**
     * Method used to retrieve the state of an android location.
     * @return
     */
    public AndroidLocation getMyValue() {
        if (state instanceof AndroidLocation) {
            return ((AndroidLocation) state).getState();
        } else {
            return null;
        }
    }


    /**
     * Method used to set the state of an Android Location.
     * @param myValue
     */
    public void setMyValue(AndroidLocation myValue) {
        AndroidLocation newState = new AndroidLocation();
        newState.setState(myValue);
        state = newState;
    }
}
