package uk.ac.uea.Framework;

import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

/**
 * Created by Josh on 18/01/2016.
 */
public class Compass {

    static Compass compass;
    AndroidLocation userLoc;
    AndroidLocation endLoc;
    float bearTo;
    private GeomagneticField geoField;
    boolean isSetup;
    // define the display assembly compass picture
    private ImageView image;

    // record the compass picture angle turned
    private float currentDegree = 0f;


    private Compass()
    {
        isSetup = false;
    }

    public static Compass getInstance()
    {
        if(compass == null)
        {
            compass = new Compass();
        }

        return compass;
    }

    public boolean setupCompass(AndroidLocation start, AndroidLocation end, ImageView image)
    {
        if(compass == null)
            return false;

        userLoc = start;
        endLoc = end;
        this.image = image;

        updateGeoField(userLoc, endLoc);

        isSetup = true;

        return true;

    }

    private void updateGeoField(AndroidLocation start, AndroidLocation end)
    {
        geoField = new GeomagneticField(Double.valueOf(
                userLoc.getLatitude()).floatValue(), Double.valueOf(
                userLoc.getLongitude()).floatValue(), Double.valueOf(
                userLoc.getAltitude()).floatValue(),
                System.currentTimeMillis());

        bearTo = userLoc.bearingTo(endLoc);
        if (bearTo < 0) {
            bearTo = bearTo + 360;
        }
    }

    public void updateUserLoc(AndroidLocation userLoc)
    {
        if(isSetup)
        {
           updateGeoField(userLoc, endLoc);
        }

    }

    public void setVisible()
    {
        //Make the compass visible
        if(isSetup) {
            image.setAlpha(1f);
            image.setVisibility(View.VISIBLE);
        }
    }


    public void setInvisible()
    {
        //Make the compass hidden
        if(isSetup) {
            image.setAlpha(0f);
            image.setVisibility(View.INVISIBLE);
        }
    }

    public void onSensorChanged(SensorEvent event) {

        if(isSetup) {
            float degree = Math.round(event.values[0]);
            if (geoField != null)
                degree -= geoField.getDeclination();
            float direction = bearTo - degree;
            if (direction < 0) {
                direction = direction + 360;
            }
            direction = direction % 360;
            currentDegree = direction;
            final RotateAnimation rotateAnim = new RotateAnimation(currentDegree, -direction,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);

            rotateAnim.setDuration(210);
            rotateAnim.setFillAfter(true);
            image.startAnimation(rotateAnim);
        }
    }

    public void changeDestination(AndroidLocation end)
    {
        endLoc = end;
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

        //Handle this?

    }
}
