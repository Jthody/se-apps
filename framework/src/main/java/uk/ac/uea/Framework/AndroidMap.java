package uk.ac.uea.Framework;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * This class handles all map related tasks
 */
public class AndroidMap implements Serializable {

    private GoogleMap googleMap;
    static boolean hasInstance;
    static AndroidMap map;
    private AndroidLocation currentAndroidLocation;
    private Polyline mLine;
    String distance;
    String duration;

    /**
     * Private constructor that is called when the map instance is null
     * @param m The GoogleMap object
     */
    private AndroidMap(GoogleMap m) {
        googleMap = m;
        currentAndroidLocation = new AndroidLocation();
        hasInstance = false;
        mLine = null;

    }

    /**
     * Method to check if there is an instance of the AnddroidMap class
     * @return
     */
    public static boolean doesHaveInstance() {
        return hasInstance;
    }

    /**
     * Singleton method to return the instance of AndroidMap
     * @return
     */
    public static AndroidMap getMap() {
        //If there is an instance of AndroidMap
        if (hasInstance) {
            return map;
        } else {
            return null;
        }
    }

    /**
     * Method to add a default marker to the Map
     * @param l Location to add the marker
     * @param message Message to put on the Map at the location
     */
    public void addMarker(AndroidLocation l, String message) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(l.getLatitude(), l.getLongitude())).title(message).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
    }

    /**
     * Method to add a blue marker to the Map
     * @param l Location to add the map
     * @param title Title to put on the Map at the location
     */
    public void addMarkerBlue(AndroidLocation l, String title) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(l.getLatitude(), l.getLongitude())).title(title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }

    /**
     * Method to add a marker to the Map
     * @param l Location to add the marker
     * @param title Title of the marker
     * @param description Description of the Location
     */
    public void addMarker(AndroidLocation l, String title, String description) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(l.getLatitude(), l.getLongitude())).snippet(description).title(title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
    }

    /**
     * This method removes all markers from the Map
     */
    public void removeMarker(){
        //.remove will remove a single marker but because markers are set up as map locations it doesn't work.
        //marker.remove();
        //Clears everything on the map which is okay for the parking aid as it only ever has one marker but shouldnt be used
        //for other apps.
        googleMap.clear();
    }


    /**
     * This method moves the camera to a specific location on the map
     * @param l Location to move the camera
     */
    public void moveCamera(AndroidLocation l) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), 18));

    }

    /**
     * This method moves the camera to a specific location with a zoom level
     * @param l Location to move the camera
     * @param zoom Zoom level
     */
    public void moveCamera(AndroidLocation l, int zoom) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), zoom));
    }

    /**
     * Method to tell the map to use the users location
     * @param b true/false
     */
    public void setMyLocationEnabled(boolean b) {
        if(map != null)
            googleMap.setMyLocationEnabled(b);
    }

    /**
     * Method to tell the map to display a compass
     * @param b true/false
     */
    public void setCompasEnabled(boolean b)
    {
        googleMap.getUiSettings().setCompassEnabled(b);
    }

    /**
     * Method that takes a Long/Lat and returns an address String
     * @param LATITUDE
     * @param LONGITUDE
     * @param context
     * @return Address String
     */
    public String getCompleteAddressString(double LATITUDE, double LONGITUDE, Context context) {

        String strAdd = "";

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress
                            .append(returnedAddress.getAddressLine(i)).append(
                            ",");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                strAdd = "No Address returned!";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    /**
     * Method that sets the GoogleMap instance for this class.
     * @param m
     */
    public static void setMap(GoogleMap m) {
        if (!hasInstance) {
            map = new AndroidMap(m);
            hasInstance = true;
        }
    }

    /**
     * Getter for the GoogleMap object
     * @return
     */
    public GoogleMap getGoogleMap() {
        return googleMap;
    }


    /**
     * Checks if the user is at a specific location
     * @param l Location to check
     * @return true if user is less than 5 meters away
     */
    public boolean isUserAtLocation(AndroidLocation l)
    {
        if(distanceToLocation(l) < 5)
            return true;

        return false;
    }

    /**
     * Method to get the users current location
     * @return users current location
     */
    public AndroidLocation getCurrentAndroidLocation() {

        if (!hasInstance)
            try {
                throw new Exception("Error: Map is not set up");
            } catch (Exception e) {
                e.printStackTrace();
            }

        return currentAndroidLocation;
    }

    /**
     * Update the map to the users current location
     */
    public void update() {
        try {
            moveCamera(getCurrentAndroidLocation());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the distance between the user and a passed Location
     * @param l Location to check the distance to
     * @return
     */
    public double distanceToLocation(AndroidLocation l) {

        AndroidLocation current = getCurrentAndroidLocation();

        return distanceBetweenLocations(current, l);
    }

    /**
     * Method to determine the distance between two Locations
     * @param a Location a
     * @param b Location b
     * @return distance between Location a and Location b
     */
    public static double distanceBetweenLocations(AndroidLocation a, AndroidLocation b) {

        //Create Location objects
        Location locA = new Location("");
        Location locB = new Location("");

        //Assign the Location objects the lang/long co-ords
        locA.setLongitude(a.getLongitude());
        locA.setLatitude(a.getLatitude());
        locB.setLongitude(b.getLongitude());
        locB.setLatitude(b.getLatitude());

        //Use the distanceTo method in the Location class
        return locA.distanceTo(locB);

    }

    /**
     * Method to clear the Map
     */
    public void clear()
    {
        if(googleMap != null)
            googleMap.clear();
    }

    /**
     * Remove the polylines drawn on the Map
     */
    public void removePolylines()
    {

        if(mLine != null) {
            mLine.remove();
        }


    }

    /**
     * Update the users current locaiton
     * @param l the users current Location
     */
    public void updateLocation(AndroidLocation l) {
        currentAndroidLocation = l;
    }

    /**
     * Method to find the closest location to the user from a List of locations
     * @param list List of locations to check
     * @return closest Location to the user
     */
    public AndroidLocation findClosestLocation(ArrayList<AndroidLocation> list)
    {
        double dist = Double.MAX_VALUE;
        AndroidLocation closest = null;

        for(AndroidLocation l : list) {
            double temp = distanceToLocation(l);
            if (temp < dist) {
                dist = temp;
                closest = l;
            }
        }

        return closest;
    }

    /**
     * Method to get the URL for the Google Directions API
     * @param origin Start Location
     * @param dest End Location
     * @param mode Travel mode
     * @return
     */
    private String getDirectionsUrl(AndroidLocation origin, AndroidLocation dest, String mode) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + ","
                + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();
        String travelMode = "mode="+mode;

        // Sensor enabled
        String sensor = "sensor=false";


        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + travelMode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;

        return url;
    }

    /**
     * Method to get the URL for the Google Directions API moving through a list of waypoints
     * @param origin Start location
     * @param dest End Location
     * @param waypointsList List of locations the route has to visit
     * @param mode Travel mode
     * @return
     */
    private String getDirectionsUrl(AndroidLocation origin, AndroidLocation dest ,ArrayList<AndroidLocation> waypointsList, String mode) {

        String str_origin = "origin=" + origin.getLatitude() + ","
                + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();

        String waypoints = "waypoints=optimize:true";

        for(AndroidLocation l : waypointsList)
        {
            waypoints = waypoints + "|" + l.getLatitude() + ","
                    + l.getLongitude();
        }

        String travelMode = "mode="+mode;

        // Sensor enabled
        String sensor = "sensor=false";


        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + waypoints + "&" + sensor + "&" + travelMode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;

        return url;

    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {

            //  Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * Method to draw the route to a location on the Map
     * @param destination End location
     * @param mode Travel mode
     */
    public void routeTo(AndroidLocation destination, String mode)
    {
        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(currentAndroidLocation, destination, mode);


        new DownloadTask().execute(url);
    }

    /**
     * Method to draw a route to a location that passes through a number of waypoints
     * @param destination Destination Location
     * @param waypoints List of waypoint Locations
     * @param mode Travel mode
     */
    public void routeTo(AndroidLocation destination, ArrayList<AndroidLocation> waypoints, String mode)
    {
        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(currentAndroidLocation, destination, waypoints, mode);
        new DownloadTask().execute(url);
    }

    /**
     * Class to fetch data from the google directions API
     * Credit to original author
     * Source: https://github.com/alsuhaibanyy/CGUApp/tree/master/CGUApp/src/cgu/edu/ist380/alsuhaibanyy/alghosona
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            //Invokes the thread for parsing the JSON data

            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if (result.size() < 1) {
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) { // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    if(point.get("lat") != null && point.get("lng") != null) {

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.color(Color.RED);
            }

            if(mLine != null)
                removePolylines();

            mLine = googleMap.addPolyline(lineOptions);
            mLine.setWidth(7);
        }
    }

}




