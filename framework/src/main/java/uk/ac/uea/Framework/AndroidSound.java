package uk.ac.uea.Framework;

import android.media.SoundPool;


/**
 * This class is an implementation of the Sound interface used to make an instance of a platform
 * specific sound.
 */
public class AndroidSound implements Sound {
    int soundId;
    SoundPool soundPool;

    /**
     * Constructor for the AndroidSound
     * @param soundPool A Collection of sounds
     * @param soundId An integer representing the id of the sound
     */
    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    /**
     * Method overriden from the Sound framework- used to play a sound at a passed volume
     * @param volume A float passed to represent the volume of the sound
     */
    @Override
    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    /**
     * Method overriden from the Sound framework- used to remove a sound from memory
     */
    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }

    /**
     * Method overriden from the Sound framework- used to stop a loaded sound.
     */
    @Override
    public void stop(){ soundPool.stop(soundId);}

}
