package uk.ac.uea.Framework;

/**
 * Abstract class used to define a Format Strategy
 */
public abstract class FormatStrategy {

    /**
     * Abstract method used to format a string.
     * @param Heading
     * @param Body
     * @return
     */
    public abstract String Format(String Heading, String Body);
}
