package uk.ac.uea.Framework;

/**
 * This interface can be implemented to create an instance of platform specific sound/music
 */
public interface Audio {

    /**
     *Method to create music
     * @param file the file location of the music
     * @return created music of type Music
     */
    public Music createMusic(String file);

    /**
     *Method to create a sound
     * @param file the file location of the sound
     * @return created sound of type Sound
     */
    public Sound createSound(String file);
}
