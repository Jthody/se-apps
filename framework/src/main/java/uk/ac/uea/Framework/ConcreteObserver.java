package uk.ac.uea.Framework;

import java.util.Observable;

/**
 * class that handles the different type of subject states using generics.
 * @param <T>
 */
public final class ConcreteObserver<T> extends Observer implements java.util.Observer {

    private ConcreteSubject<T> MySubject;
    private ObserverClient<T> MyClient;

    /**
     * Constructor used to create an ConcreteObserver object and sets MySubject and MyClient.
     * @param Subject
     * @param Client
     */
    public ConcreteObserver(ConcreteSubject<T> Subject, ObserverClient<T> Client) {
        MySubject = Subject;
        MySubject.Attach(this);
        MyClient = Client;
    }

    /**
     * Method used to remove the current state of MySubject.
     */
    public void Detach() {
        MySubject.Detach(this);
    }

    /**
     * Method used to update the MyClient object.
     */
    @Override
    public void Update() {
        MyClient.Update(MySubject.GetState());
    }

    /**
     * Method used to update
     * @param observable
     * @param data
     */
    @Override
    public void update(Observable observable, Object data) {

    }
}