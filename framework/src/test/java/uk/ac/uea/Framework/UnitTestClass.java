package uk.ac.uea.Framework;

import android.location.Location;
import android.widget.ImageView;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * Class to run the Unit tests for our apps.
 */
public class UnitTestClass {

    /**
     * unit test to check if the distance between two locations is correct
     * @throws Exception
     */
    @Test
    public void distanceTest_isCorrect() throws Exception {

        AndroidLocation a, b;

        //Mock the AndroidLocation objects
        a = new AndroidLocation(Mockito.mock(Location.class));
        b = new AndroidLocation(Mockito.mock(Location.class));

        //As both locations have a long/lat of 0 then distance should be 0
        assertEquals(AndroidMap.distanceBetweenLocations(a, b), 0, 0);

    }

    @Test
    public void bearingTest_isCorrect() throws Exception {
        AndroidLocation a,b;

        //Mock the AndroidLocation objects
        a = new AndroidLocation(Mockito.mock(Location.class));
        b = new AndroidLocation(Mockito.mock(Location.class));

        //As both locations have a long/lat of 0 then bearing between them should be 0
        assertEquals(a.bearingTo(b), 0, 0);
        assertEquals(b.bearingTo(a), 0, 0);

    }

    @Test
    public void setUpCompass_isCorrect() throws Exception
    {
        AndroidLocation a,b;
        Compass compass;

        //Mock the AndroidLocation objects
        a = new AndroidLocation(Mockito.mock(Location.class));
        b = new AndroidLocation(Mockito.mock(Location.class));

        compass = Compass.getInstance();

        //Check if the compass can be set up and point the user to a location
        assertEquals(compass.setupCompass(a, b, Mockito.mock(ImageView.class)), true);
    }




}